﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Shop.AdministrationWPF.UserControls
{
    /// <summary>
    /// Interaction logic for ImportProductsControl.xaml
    /// </summary>
    public partial class ImportProductsControl : UserControl
    {
        public ImportProductsControl()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtFileLoaded.Text))
            {
                OpenFileDialog dlg = new OpenFileDialog();

                dlg.DefaultExt = ".xls";
                dlg.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

                bool? result = dlg.ShowDialog();

                if (result == true)
                {
                    // Open document 
                    string filename = dlg.FileName;
                    txtFileLoaded.Text = filename;
                }
            }
            else
            {
                //TODO: import z pliku
            }
        }
    }
}
