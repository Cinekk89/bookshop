namespace Shop.DataAccess.Migrations
{
    using Core;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<BookShopContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BookShopContext context)
        {
            // Product types seed
            var ptype1 = new ProductType() { Type = "AudioBook" };
            var ptype2 = new ProductType() { Type = "E-Book" };

            // Authors seed

            var author1 = new Author() { Name = "Miros�aw", Surname = "Jab�o�ski" };
            var author2 = new Author() { Name = "E. E. Doc", Surname = "Smith" };
            var author3 = new Author() { Name = "Konrad", Surname = "Fia�kowski" };
            var author4 = new Author() { Name = "Maciej", Surname = "Musialik" };
            var author5 = new Author() { Name = "Czes�aw", Surname = "Bia�czy�ski" };
            var author6 = new Author() { Name = "Marcin", Surname = "Wolski" };
            var author7 = new Author() { Name = "Wojciech", Surname = "M�ynarski" };
            var author8 = new Author() { Name = "Robert", Surname = "Harris" };
            var author9 = new Author() { Name = "Alex", Surname = "Kava" };
            var author10 = new Author() { Name = "Carrisi", Surname = "Donato" };
            var author11 = new Author() { Name = "Jo", Surname = "Nesbo" };

            // Publishing houses seed
            var phouse1 = new PublishingHouse() { Name = "Level Trading" };
            var phouse2 = new PublishingHouse() { Name = "Fabryka s��w" };
            var phouse3 = new PublishingHouse() { Name = "Sonia Draga" };
            var phouse4 = new PublishingHouse() { Name = "WAB" };

            // Books seed
            var books = new List<Book>()
                {
                    new Book() { Name = "Sierpem i m�otem", ReleaseDate = new DateTime(2010, 02, 10), Price = 20.99M, Author = author1,
                        IsGoodProduct = true, PublishingHouse = phouse1, Type = ptype1 },

                    new Book() { Name = "Skylark 2", ReleaseDate = new DateTime(2008, 02, 20), Price = 15.20M, Author = author2,
                        IsGoodProduct = false, PublishingHouse = phouse2, Type = ptype1 },

                    new Book() { Name = "Nie�miertelny z Wegi", ReleaseDate = new DateTime(2012, 01, 31), Price = 30.00M, Author = author3,
                        IsGoodProduct = false, PublishingHouse = phouse3, Type = ptype2 },

                    new Book() { Name = "Zanie�my im nasz� wojn�", ReleaseDate = new DateTime(2013, 02, 19), Price = 10.99M, Author = author4,
                        IsGoodProduct = false, PublishingHouse = phouse4, Type = ptype1 },

                    new Book() { Name = "�y� i umrze� w Nowej Moskwie", ReleaseDate = new DateTime(2000, 02, 22), Price = 9.99M, Author = author4,
                        IsGoodProduct = false, PublishingHouse = phouse1, Type = ptype2 },

                    new Book() { Name = "Wojny Hoaltaskie", ReleaseDate = new DateTime(1999, 02, 23), Price = 50.00M, Author = author5,
                        IsGoodProduct = false, PublishingHouse = phouse3, Type = ptype1 },

                    new Book() { Name = "Kwadratura tr�jk�ta", ReleaseDate = new DateTime(2007, 02, 24), Price = 30.00M, Author = author5,
                        IsGoodProduct = false, PublishingHouse = phouse4, Type = ptype2 },

                    new Book() { Name = "Stara Ziemia", ReleaseDate = new DateTime(2010, 02, 16), Price = 14.99M, Author = author6,
                        IsGoodProduct = false, PublishingHouse = phouse1, Type = ptype1 },

                    new Book() { Name = "Skylark w kosmosie", ReleaseDate = new DateTime(2011, 02, 02), Price = 13.00M, Author = author7,
                        IsGoodProduct = false, PublishingHouse = phouse4, Type = ptype2 },

                    new Book() { Name = "Plazmoid", ReleaseDate = new DateTime(2012, 02, 15), Price = 35.00M, Author = author8,
                        IsGoodProduct = false, PublishingHouse = phouse2, Type = ptype1 },

                    new Book() { Name = "Gra o tron", ReleaseDate = new DateTime(2016, 09, 12), Price = 59.99M, Author = author9,
                        IsGoodProduct = false, PublishingHouse = phouse2, Type = ptype2 },

                    new Book() { Name = "Jakub Wendrowycz", ReleaseDate = new DateTime(2017, 03, 27), Price = 18.50M, Author = author6,
                        IsGoodProduct = false, PublishingHouse = phouse1, Type = ptype1 },

                    new Book() { Name = "�owca Cieni", ReleaseDate = new DateTime(2017, 04, 03), Price = 23.00M, Author = author10,
                        IsGoodProduct = false, PublishingHouse = phouse4, Type = ptype2 },

                    new Book() { Name = "Harry Hole", ReleaseDate = new DateTime(2017, 03, 24), Price = 35.00M, Author = author11,
                        IsGoodProduct = false, PublishingHouse = phouse3, Type = ptype1 },

                    new Book() { Name = "Wied�min", ReleaseDate = new DateTime(2017, 09, 12), Price = 50.00M, Author = author3,
                        IsGoodProduct = true, PublishingHouse = phouse1, Type = ptype2 }
                };

            context.Books.AddRange(books);

            // Carriers seed
            var carriers = new List<Carrier>()
                {
                    new Carrier() { Name = "PenDrive" },
                    new Carrier() { Name = "P�yta CD" },
                    new Carrier() { Name = "P�yta DVD" }
                };

            context.Carriers.AddRange(carriers);
        }
    }
}
