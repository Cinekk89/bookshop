namespace Shop.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BookShopMig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.t_Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Surname = c.String(maxLength: 70),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.t_Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                        ReleaseDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Price = c.Decimal(nullable: false, precision: 5, scale: 2),
                        AuthorId = c.Int(nullable: false),
                        ProductTypeId = c.Int(nullable: false),
                        IsGoodProduct = c.Boolean(nullable: false),
                        PublishingHouseId = c.Int(nullable: false),
                        isDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.t_PublishingHouses", t => t.PublishingHouseId, cascadeDelete: true)
                .ForeignKey("dbo.t_ProductTypes", t => t.ProductTypeId, cascadeDelete: true)
                .ForeignKey("dbo.t_Authors", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.Name)
                .Index(t => t.AuthorId)
                .Index(t => t.ProductTypeId)
                .Index(t => t.PublishingHouseId);
            
            CreateTable(
                "dbo.t_PublishingHouses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.t_ProductTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.t_Carriers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.t_Books", "AuthorId", "dbo.t_Authors");
            DropForeignKey("dbo.t_Books", "ProductTypeId", "dbo.t_ProductTypes");
            DropForeignKey("dbo.t_Books", "PublishingHouseId", "dbo.t_PublishingHouses");
            DropIndex("dbo.t_Books", new[] { "PublishingHouseId" });
            DropIndex("dbo.t_Books", new[] { "ProductTypeId" });
            DropIndex("dbo.t_Books", new[] { "AuthorId" });
            DropIndex("dbo.t_Books", new[] { "Name" });
            DropTable("dbo.t_Carriers");
            DropTable("dbo.t_ProductTypes");
            DropTable("dbo.t_PublishingHouses");
            DropTable("dbo.t_Books");
            DropTable("dbo.t_Authors");
        }
    }
}
