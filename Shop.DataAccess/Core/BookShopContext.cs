﻿using Shop.DataAccess.Mappings;
using Shop.DataAccess.Models;
using System.Data.Entity;

namespace Shop.DataAccess.Core
{
    public class BookShopContext : DbContext
    {
        public BookShopContext() : base("BookShopDB")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BookShopContext, Migrations.Configuration>("BookShopDB"));
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<PublishingHouse> PublishingHouses { get; set;}
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Carrier> Carriers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AuthorMapping());
            modelBuilder.Configurations.Add(new BookMapping());
            modelBuilder.Configurations.Add(new PublishingHouseMapping());
            modelBuilder.Configurations.Add(new ProductTypeMapping());
            modelBuilder.Configurations.Add(new CarrierMapping());
        }
    }
}
