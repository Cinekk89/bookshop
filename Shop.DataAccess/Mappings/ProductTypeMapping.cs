﻿using Shop.DataAccess.Models;
using System.Data.Entity.ModelConfiguration;

namespace Shop.DataAccess.Mappings
{
    public class ProductTypeMapping : EntityTypeConfiguration<ProductType>
    {
        public ProductTypeMapping()
        {
            this.ToTable("t_ProductTypes");
            this.HasKey(c => c.Id);
            this.HasMany(p => p.Books).WithRequired(b => b.Type).HasForeignKey(b => b.ProductTypeId);
        }
    }
}
