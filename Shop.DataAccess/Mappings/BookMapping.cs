﻿using Shop.DataAccess.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Shop.DataAccess.Mappings
{
    public class BookMapping : EntityTypeConfiguration<Book>
    {
        public BookMapping()
        {
            this.ToTable("t_Books");
            this.HasKey(b => b.Id);
            this.HasRequired(b => b.Author);
            this.Property(b => b.Price).HasPrecision(5, 2);
            this.Property(b => b.Name).HasMaxLength(300).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.HasRequired(b => b.PublishingHouse);
            this.Property(b => b.ReleaseDate).HasColumnType("datetime2");
            this.HasRequired(b => b.Type);
            this.Property(b => b.ProductTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
        }
    }
}
