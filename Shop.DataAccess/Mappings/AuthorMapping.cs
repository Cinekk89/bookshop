﻿using Shop.DataAccess.Models;
using System.Data.Entity.ModelConfiguration;

namespace Shop.DataAccess.Mappings
{
    public class AuthorMapping : EntityTypeConfiguration<Author>
    {
        public AuthorMapping()
        {
            this.ToTable("t_Authors");
            this.HasKey(a => a.Id);
            this.Property(a => a.Name).HasMaxLength(50);
            this.Property(a => a.Surname).HasMaxLength(70);
            this.HasMany(a => a.Books).WithRequired(b => b.Author).HasForeignKey(b => b.AuthorId);
        }
    }
}
