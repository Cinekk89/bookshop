﻿using Shop.DataAccess.Models;
using System.Data.Entity.ModelConfiguration;

namespace Shop.DataAccess.Mappings
{
    public class PublishingHouseMapping : EntityTypeConfiguration<PublishingHouse>
    {
        public PublishingHouseMapping()
        {
            this.ToTable("t_PublishingHouses");
            this.HasKey(p => p.Id);
            this.Property(b => b.Name).HasMaxLength(300);
            this.HasMany(p => p.Books).WithRequired(b => b.PublishingHouse).HasForeignKey(b => b.PublishingHouseId);
        }
    }
}
