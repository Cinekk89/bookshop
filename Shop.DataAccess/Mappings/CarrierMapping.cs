﻿using Shop.DataAccess.Models;
using System.Data.Entity.ModelConfiguration;

namespace Shop.DataAccess.Mappings
{
    public class CarrierMapping : EntityTypeConfiguration<Carrier>
    {
        public CarrierMapping()
        {
            this.ToTable("t_Carriers");
            this.HasKey(c => c.Id);
        }
    }
}
