﻿using System.Collections.Generic;

namespace Shop.DataAccess.Models
{
    public class ProductType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}
