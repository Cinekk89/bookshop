﻿using System.Collections.Generic;

namespace Shop.DataAccess.Models
{
    public class PublishingHouse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
