﻿using System;

namespace Shop.DataAccess.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime ReleaseDate { get; set; }

        public decimal Price { get; set; }

        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }

        public int ProductTypeId { get; set; }

        public virtual ProductType Type { get; set; }

        public bool IsGoodProduct { get; set; }

        public int PublishingHouseId { get; set; }

        public virtual PublishingHouse PublishingHouse { get; set; }

        public bool isDeleted { get; set; }
    }
}
