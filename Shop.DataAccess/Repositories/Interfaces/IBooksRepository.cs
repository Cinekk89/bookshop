﻿using Shop.DataAccess.Infrastructure;
using Shop.DataAccess.Models;
using System.Collections.Generic;

namespace Shop.DataAccess.Repositories.Interfaces
{
    public interface IBooksRepository
    {
        List<Book> GetAllBooks();
        List<Book> GetFilteredBooks(BooksQuery query, string additionalWhereClause);
        Book GetBook(int bookId);
        List<Carrier> GetCarriersList();
    }
}
