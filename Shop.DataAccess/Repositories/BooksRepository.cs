﻿using Shop.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shop.DataAccess.Models;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;
using Shop.DataAccess.Infrastructure;
using System;

namespace Shop.DataAccess.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        private readonly string connectionString;
        private WhereClauseBuilder _builder;

        public BooksRepository()
        {
            connectionString = @"Data Source =.\SQLEXPRESS; Initial Catalog = BookShopDB; Integrated Security = True";
            var wrongConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            _builder = new WhereClauseBuilder();
        }

        public List<Book> GetAllBooks()
        {
            List<Book> books = null;
            using (var con = new SqlConnection(connectionString))
            {
                books = con.Query<Book>("SELECT * FROM [dbo].[t_Books] Order By Name").ToList();

                foreach (var book in books)
                {
                    book.Author = con.Query<Author>("SELECT * FROM[dbo].[t_Authors] WHERE Id = " + book.AuthorId).First();
                    book.PublishingHouse = con.Query<PublishingHouse>("SELECT * FROM[dbo].[t_PublishingHouses] WHERE Id = " + book.PublishingHouseId).First();
                }

                con.Close();
            }

            return books;
        }

        public Book GetBook(int bookId)
        {
            Book book = null;
            using (var con = new SqlConnection(connectionString))
            {
                book = con.Query<Book>("SELECT * FROM [dbo].[t_Books] WHERE Id = " + bookId).FirstOrDefault();

                book.Author = con.Query<Author>("SELECT * FROM[dbo].[t_Authors] WHERE Id = " + book.AuthorId).First();
                book.PublishingHouse = con.Query<PublishingHouse>("SELECT * FROM[dbo].[t_PublishingHouses] WHERE Id = " + book.PublishingHouseId).First();

                con.Close();
            }

            return book;
        }

        public List<Carrier> GetCarriersList()
        {
            List<Carrier> carriers = null;
            using (var con = new SqlConnection(connectionString))
            {
                carriers = con.Query<Carrier>("SELECT * FROM [dbo].[t_Carriers]").ToList();

                con.Close();
            }

            return carriers;
        }

        public List<Book> GetFilteredBooks(BooksQuery query, string additionalWhereClause)
        {
            StringBuilder whereClause = new StringBuilder();
            

            var isFiltredByName = false;
            var isFilteredByType = false;
            // Name filter
            if (!string.IsNullOrEmpty(query.Name))
            {
                _builder.AppendWhereClause()
                    .Append("(b.Name").AppendLikeClause(query.Name)
                    .AppendOrClause()
                    .Append("(a.Name + ' ' + a.Surname)").AppendLikeClause(query.Name).Append(")");
                isFiltredByName = true;
            }
            //additional conditions defined for products
            if (!string.IsNullOrEmpty(additionalWhereClause))
            {
                if (isFiltredByName)
                    _builder.AppendAndClause();
                else
                    _builder.AppendWhereClause();
                _builder.Append(additionalWhereClause);

                isFilteredByType = true;
            }

            if (!isFilteredByType && !isFiltredByName)
                _builder.AppendWhereClause();
            else
                _builder.AppendAndClause();
            _builder.Append("b.IsDeleted").AppendEqaulClause().Append("0");

            var conditionToAdd = _builder.GetWhereClause().ToString();

            List<Book> books = null;
            using (var con = new SqlConnection(connectionString))
            {
                books = con.Query<Book>("SELECT b.Id, b.Name, b.ReleaseDate, b.Price, b.AuthorId, b.ProductTypeId, b.IsGoodProduct, b.PublishingHouseId, b.IsDeleted " +
                    "FROM [BookShopDB].[dbo].[t_Books] b JOIN [dbo].[t_Authors] a ON b.AuthorId = a.Id" + conditionToAdd + " Order By b.Name").ToList();

                foreach (var book in books)
                {
                    book.Author = con.Query<Author>("SELECT * FROM [BookShopDB].[dbo].[t_Authors] WHERE Id = " + book.AuthorId).First();
                    book.PublishingHouse = con.Query<PublishingHouse>("SELECT * FROM[dbo].[t_PublishingHouses] WHERE Id = " + book.PublishingHouseId).First();
                }

                con.Close();
            }

            return books;
        }
    }
}
