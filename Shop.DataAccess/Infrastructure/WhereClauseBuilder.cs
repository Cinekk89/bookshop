﻿using System.Text;

namespace Shop.DataAccess.Infrastructure
{
    public class WhereClauseBuilder
    {
        private StringBuilder _whereClauseBuilder;
        private const string WhereClause = " WHERE ";
        private const string AndClause = " AND ";
        private const string OrClause = " OR ";
        private const string EqualClause = " = ";
        private const string LikeClause = " LIKE ";


        public WhereClauseBuilder()
        {
            _whereClauseBuilder = new StringBuilder();
        }

        public WhereClauseBuilder Append(string literal, bool appendApostrophe = false)
        {
            if (appendApostrophe)
                literal = "'" + literal + "' ";

            _whereClauseBuilder.Append(literal + " ");

            return this;
        }

        public WhereClauseBuilder AppendWhereClause()
        {
            _whereClauseBuilder.Append(WhereClause);

            return this;
        }

        public WhereClauseBuilder AppendAndClause()
        {
            _whereClauseBuilder.Append(AndClause);

            return this;
        }

        public WhereClauseBuilder AppendOrClause()
        {
            _whereClauseBuilder.Append(OrClause);

            return this;
        }

        public WhereClauseBuilder AppendEqaulClause()
        {
            _whereClauseBuilder.Append(EqualClause);

            return this;
        }

        public StringBuilder GetWhereClause()
        {
            return _whereClauseBuilder;
        }

        public WhereClauseBuilder AppendLikeClause(string value)
        {
            _whereClauseBuilder.Append(LikeClause + "'%" + value + "%'");

            return this;
        }
    }
}
