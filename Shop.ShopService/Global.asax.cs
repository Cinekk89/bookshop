﻿using Shop.DataAccess.Repositories;
using Shop.DataAccess.Repositories.Interfaces;
using Shop.ShopService.Services;
using Shop.ShopService.Services.Interfaces;
using Shop.ShopService.Utilities;
using Shop.ShopService.Utilities.Interfaces;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Shop.ShopService
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            // Register your types, for instance:
            RegisterDependencies(container);
            //container.Register<IUnitOfWork, EfUnitOfWork>(Lifestyle.Scoped);

            // Register the container to the SimpleInjectorServiceHostFactory.
            SimpleInjectorServiceHostFactory.SetContainer(container);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        private void RegisterDependencies(Container container)
        {
            container.Register<IBooksRepository, BooksRepository>();
            container.Register<IBooksService, BooksService>();
            container.Register<IConverterUtil, ConverterUtil>();
        }
    }
}