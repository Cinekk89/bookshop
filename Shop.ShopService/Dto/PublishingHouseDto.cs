﻿using System.Runtime.Serialization;

namespace Shop.ShopService.Dto
{
    [DataContract]
    public class PublishingHouseDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}