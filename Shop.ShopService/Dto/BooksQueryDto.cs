﻿using System.Runtime.Serialization;

namespace Shop.ShopService.Dto
{
    [DataContract]
    public class BooksQueryDto
    {
        [DataMember]
        public string Name { get; set; }
    }
}