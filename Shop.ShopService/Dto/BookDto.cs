﻿using System;
using System.Runtime.Serialization;

namespace Shop.ShopService.Dto
{
    [DataContract]
    public class BookDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime ReleaseDate { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int AuthorId { get; set; }

        [DataMember]
        public virtual AuthorDto Author { get; set; }

        [DataMember]
        public int ProductTypeId { get; set; }

        //public virtual ProductType Type { get; set; }

        [DataMember]
        public bool IsGoodProduct { get; set; }

        [DataMember]
        public int PublishingHouseId { get; set; }

        [DataMember]
        public virtual PublishingHouseDto PublishingHouse { get; set; }
    }
}