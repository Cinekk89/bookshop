﻿using System.Runtime.Serialization;

namespace Shop.ShopService.Dto
{
    [DataContract]
    public class AuthorDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }
    }
}