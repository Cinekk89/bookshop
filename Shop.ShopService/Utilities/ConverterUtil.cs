﻿using Shop.ShopService.Utilities.Interfaces;
using Shop.DataAccess.Models;
using Shop.ShopService.Dto;
using Shop.DataAccess.Infrastructure;
using System;

namespace Shop.ShopService.Utilities
{
    public class ConverterUtil : IConverterUtil
    {
        public BookDto ConvertToDto(Book book)
        {
            return new BookDto()
            {
                Id = book.Id,
                Name = book.Name,
                Price = book.Price,
                ReleaseDate = book.ReleaseDate,
                AuthorId = book.AuthorId,
                PublishingHouseId = book.PublishingHouseId,
                IsGoodProduct = book.IsGoodProduct,
                ProductTypeId = book.ProductTypeId,
                PublishingHouse = ConvertToDto(book.PublishingHouse),
                Author = ConvertToDto(book.Author)
            };
        }

        public BooksQuery ConvertToBase(BooksQueryDto dto)
        {
            return new BooksQuery()
            {
                Name = dto.Name
            };
        }

        public AuthorDto ConvertToDto(Author author)
        {
            return new AuthorDto()
            {
                Id = author.Id,
                Name = author.Name,
                Surname = author.Surname
            };
        }

        public Author ConvertToBase(AuthorDto dto)
        {
            return new Author()
            {
                Id = dto.Id,
                Name = dto.Name,
                Surname = dto.Surname
            };
        }

        public PublishingHouseDto ConvertToDto(PublishingHouse phouse)
        {
            return new PublishingHouseDto()
            {
                Id = phouse.Id,
                Name = phouse.Name
            };
        }

        public PublishingHouse ConvertToBase(PublishingHouseDto dto)
        {
            return new PublishingHouse()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public CarrierDto ConvertToDto(Carrier carrier)
        {
            return new CarrierDto()
            {
                Id = carrier.Id,
                Name = carrier.Name
            };
        }
    }
}