﻿using Shop.DataAccess.Infrastructure;
using Shop.DataAccess.Models;
using Shop.ShopService.Dto;

namespace Shop.ShopService.Utilities.Interfaces
{
    public interface IConverterUtil
    {
        BookDto ConvertToDto(Book book);
        BooksQuery ConvertToBase(BooksQueryDto dto);
        AuthorDto ConvertToDto(Author author);
        Author ConvertToBase(AuthorDto dto);
        PublishingHouseDto ConvertToDto(PublishingHouse phouse);
        PublishingHouse ConvertToBase(PublishingHouseDto dto);
        CarrierDto ConvertToDto(Carrier carrier);
    }
}
