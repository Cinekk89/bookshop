﻿using Shop.ShopService.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Shop.DataAccess.Repositories.Interfaces;
using Shop.ShopService.Utilities.Interfaces;
using Shop.ShopService.Dto;
using System;

namespace Shop.ShopService.Services
{
    public class BooksService : IBooksService
    {
        private readonly IBooksRepository _booksRepo;
        private readonly IConverterUtil _converter;

        public BooksService(IBooksRepository booksRepo, IConverterUtil converter)
        {
            _booksRepo = booksRepo;
            _converter = converter;
        }

        public List<BookDto> GetAllBooks()
        {

            var books = _booksRepo.GetAllBooks();

            return books.Select(b => _converter.ConvertToDto(b)).ToList();
        }

        public BookDto GetBook(int bookId)
        {
            var book = _booksRepo.GetBook(bookId);

            if(book == null)
                throw new ArgumentException("Nie znaleziono książki o podanym Id -> " + bookId);

            return _converter.ConvertToDto(book);

        }

        public List<CarrierDto> GetCarriers()
        {
            var carriers = _booksRepo.GetCarriersList();

            if (carriers == null)
                throw new Exception("Nie znaleziono żadnych nośników!");

            return carriers.Select(c => _converter.ConvertToDto(c)).ToList();
        }

        public List<BookDto> GetFilteredBooks(BooksQueryDto dto, string additionalParams)
        {
            if (dto == null)
                dto = new BooksQueryDto();

            var books = _booksRepo.GetFilteredBooks(_converter.ConvertToBase(dto), additionalParams);

            return books.Select(b => _converter.ConvertToDto(b)).ToList();
        }
    }
}