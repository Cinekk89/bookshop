﻿using Shop.ShopService.Dto;
using System.Collections.Generic;

namespace Shop.ShopService.Services.Interfaces
{
    public interface IBooksService
    {
        List<BookDto> GetAllBooks();
        List<BookDto> GetFilteredBooks(BooksQueryDto dto, string additionalParams);
        BookDto GetBook(int bookId);
        List<CarrierDto> GetCarriers();
    }
}
