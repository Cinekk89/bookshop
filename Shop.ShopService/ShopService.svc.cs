﻿using Shop.ShopService.Dto;
using Shop.ShopService.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Shop.ShopService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ShopService : IShopService
    {
        private readonly IBooksService _booksService;
        public ShopService(IBooksService booksService)
        {
            _booksService = booksService;
        }

        public List<BookDto> GetAllBooks()
        {
            var books = _booksService.GetAllBooks();

            return books;
        }

        public BookDto GetBook(int bookId)
        {
            var book = _booksService.GetBook(bookId);

            return book;
        }

        public List<CarrierDto> GetCarriers()
        {
            var carriers = _booksService.GetCarriers();

            return carriers;
        }

        public List<BookDto> GetFilteredBooks(BooksQueryDto dto, string additionalParams)
        {
            var books = _booksService.GetFilteredBooks(dto, additionalParams);

            return books;
        }
    }
}
