﻿using Shop.ShopService.Dto;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Shop.ShopService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IShopService
    {
        [OperationContract]
        List<BookDto> GetAllBooks();

        [OperationContract]
        List<BookDto> GetFilteredBooks(BooksQueryDto dto, string additionalParams);

        [OperationContract]
        BookDto GetBook(int bookId);

        [OperationContract]
        List<CarrierDto> GetCarriers();

        // TODO: Add your service operations here
    }
}
