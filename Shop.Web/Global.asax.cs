﻿using Shop.Web.Services;
using Shop.Web.Services.Interfaces;
using Shop.Web.Utilities;
using Shop.Web.Utilities.Interfaces;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Shop.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();
            RegisterDependencies(container);


            // This two extension method from integration package
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private void RegisterDependencies(Container container)
        {
            container.Register<IBooksService, BooksService>();
            container.Register<IDtoConverter, DtoConverter>();
            var typesToRegister = container.GetTypesToRegister(typeof(IBooksFilterExecutor), new[] { typeof(IBooksFilterExecutor).Assembly });

            container.RegisterCollection(typeof(IBooksFilterExecutor), typesToRegister);
        }
    }
}
