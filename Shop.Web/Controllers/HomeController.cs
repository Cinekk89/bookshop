﻿using Shop.Web.Models;
using Shop.Web.Models.ViewModels;
using Shop.Web.Services.Interfaces;
using Shop.Web.Utilities.Enums;
using System;
using System.Web.Mvc;

namespace Shop.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBooksService _booksService;
        public BooksCartModel LayoutModel { get; set; }

        public HomeController(IBooksService booksService)
        {
            _booksService = booksService;

            var booksICart = (BooksCartModel)ViewData["booksInCart"];

            LayoutModel = System.Web.HttpContext.Current != null ? LoadLayoutModel() : new BooksCartModel();

            this.ViewData["LayoutModel"] = this.LayoutModel;
        }

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult GetBooks(eBookFilter filterType, string filter)
        {
            var bookQuery = new BooksQuery();

            bookQuery.Name = filter;

            var books = _booksService.GetFilteredBooks(bookQuery, filterType);

            var booksViewModel = new BookListModel()
            {
                Books = books
            };

            if (books.Count == 0 && !string.IsNullOrEmpty(bookQuery.Name))
            {
                return Content("false");
            }

            return PartialView("_BooksListPartial", booksViewModel);
        }

        public ActionResult OpenAddBookToCartAjax(int bookId)
        {
            var book = _booksService.GetBookForAdd(bookId);

            return PartialView("_AddBookToCartPartial", book);
        }

        public ActionResult ShowCartAjax()
        {
            return PartialView("_ShowCartPartial", LayoutModel);
        }

        [HttpPost]
        public ActionResult AddBookToCart(AddBookModel model)
        {
            if (model == null)
                throw new ArgumentException("Dane o książce niewypełnione!");

            if (model.CarrierId > 0)
            {
                return Content("false;Należy wybrać nośnik!");
            }
            if(model.Amount > 1 && model.Amount < 999)
            {
                return Content("false;Należy wybrać ilość!");
            }
            if(!string.IsNullOrEmpty(model.Name))
            {
                return Content("false;Brak nazwy wybranego produktu!");
            }
            _booksService.AddBookToCart(model, System.Web.HttpContext.Current.ApplicationInstance.Context);
            
            return RedirectToAction("Index");
        }

        private BooksCartModel LoadLayoutModel()
        {
            var layoutModel = _booksService.GetBooksInCart(System.Web.HttpContext.Current.ApplicationInstance.Context) ?? new BooksCartModel();

            return layoutModel;
        }
    }
}