﻿using Shop.Web.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Web.Models;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Infrastructure
{
    public abstract class FilterExecutorBase : IBooksFilterExecutor
    {
        protected IDtoConverter _converter;

        public FilterExecutorBase(IDtoConverter converter)
        {
            _converter = converter;
        }

        public abstract eBookFilter Filter { get; }

        public virtual List<BookModel> GetFilteredBooks(BooksQuery query)
        {
            List<BookModel> books = null;
            ShopService.ShopServiceClient client = new ShopService.ShopServiceClient();

            try
            {
                books = client.GetFilteredBooks(_converter.ConvertToDto(query), CreateAdditionalCondition()).Select(b => _converter.ConvertToModel(b)).ToList();

                client.Close();
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (client != null)
                    client.Abort();
            }

            return books;
        }

        protected virtual string CreateAdditionalCondition()
        {
            return "";
        }
    }
}