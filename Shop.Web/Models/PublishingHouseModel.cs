﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Web.Models
{
    public class PublishingHouseModel
    {
        public int Id { get; set; }

        [Display(Name="Nazwa")]
        public string Name { get; set; }
    }
}