﻿namespace Shop.Web.Models
{
    public class BooksQuery
    {
        public string Name { get; set; }

        public string AdditionalParameters { get; set; }

        public int PageSize { get; set; }
        public int Page { get; set; }
        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }
    }
}