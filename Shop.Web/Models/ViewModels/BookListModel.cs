﻿using System.Collections.Generic;

namespace Shop.Web.Models.ViewModels
{
    public class BookListModel
    {
        public BookListModel()
        {
            Books = new List<BookModel>();
        }

        public List<BookModel> Books { get; set; }
    }
}