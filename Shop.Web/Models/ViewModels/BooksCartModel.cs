﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Shop.Web.Models.ViewModels
{
    public class BooksCartModel
    {
        public BooksCartModel()
        {
            Books = new List<AddBookModel>();
            Filter = new BooksQuery();
        }

        public int TotalItems {
            get { return CountItemsICart(); } }

        public List<AddBookModel> Books { get; set; }

        public BooksQuery Filter { get; set; }

        private int CountItemsICart()
        {
            var count = 0;
            foreach(var book in Books)
            {
                count += book.Amount;
            }
            return count;
        }
    }
}