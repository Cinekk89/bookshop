﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.Web.Models.ViewModels
{
    public class AddBookModel
    {
        public int Id { get; set; }

        [Display(Name="Tytuł")]
        [Required(ErrorMessage = "Tytuł jest wymagany!")]
        public string Name { get; set; }

        [Display(Name = "Nośnik")]
        [Required(ErrorMessage = "Nośnik jest wymagany!")]
        public int CarrierId { get; set; }

        [Required]
        [Display(Name="Nośnik")]
        public string CarrierName { get; set; }
        
        public List<CarrierModel> Carriers { get; set; }

        [Display(Name = "Ilość")]
        [Range(1, 999, ErrorMessage = "Ilość musi być w przedziale 1 - 999!")]
        [RegularExpression("[0-9]{1,}",ErrorMessage = "Ilość musi być liczbą całkowitą!")]
        public int Amount { get; set; }
    }
}