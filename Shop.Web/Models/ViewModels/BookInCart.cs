﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Web.Models.ViewModels
{
    public class BookInCart
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public decimal Price { get; set; }

        public CarrierModel Carrier { get; set; }

        public int Amount { get; set; }
    }
}