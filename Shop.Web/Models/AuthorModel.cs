﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Web.Models
{
    public class AuthorModel
    {
        public int Id { get; set; }

        [Display(Name="Imię")]
        public string Name { get; set; }

        [Display(Name="Nazwisko")]
        public string Surname { get; set; }

        [Display(Name="Autor")]
        public string FullName { get; set; }
    }
}