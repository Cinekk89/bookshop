﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Web.Models
{
    public class CarrierModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}