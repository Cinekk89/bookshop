﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shop.Web.Models
{
    public class BookModel
    {
        public int Id { get; set; }
        
        [Display(Name="Tytuł")]
        public string Name { get; set; }

        [Display(Name = "Data wydania")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Cena")]
        [DisplayFormat(DataFormatString = "{0:C2}")]
        public decimal Price { get; set; }

        public int AuthorId { get; set; }

        public virtual AuthorModel Author { get; set; }

        public int ProductTypeId { get; set; }

        //public virtual ProductType Type { get; set; }

        public bool IsGoodProduct { get; set; }

        public int PublishingHouseId { get; set; }

        public PublishingHouseModel PublishingHouse { get; set; }
    }
}