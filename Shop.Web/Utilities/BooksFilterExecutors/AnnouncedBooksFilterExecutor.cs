﻿using Shop.Web.Utilities.Interfaces;
using System;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class AnnouncedBooksFilterExecutor : FilterExecutorBase
    {
        public AnnouncedBooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.Announcement;

        protected override string CreateAdditionalCondition()
        {
            var dateToCheckIfNewBook = DateTime.Now.ToString("yyyyMMdd");
            return "b.ReleaseDate > '" + dateToCheckIfNewBook + "'";
        }
    }
}