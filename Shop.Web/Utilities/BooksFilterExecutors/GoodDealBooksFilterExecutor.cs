﻿using Shop.Web.Utilities.Interfaces;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class GoodDealBooksFilterExecutor : FilterExecutorBase
    {
        public GoodDealBooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.GoodDeal;

        protected override string CreateAdditionalCondition()
        {
            return "b.IsGoodProduct = 1";
        }
    }
}