﻿using Shop.Web.Utilities.Interfaces;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class AudiobooksFilterExecutor : FilterExecutorBase
    {
        public AudiobooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.Audiobook;

        protected override string CreateAdditionalCondition()
        {
            return "b.ProductTypeId = 1";
        }
    }
}