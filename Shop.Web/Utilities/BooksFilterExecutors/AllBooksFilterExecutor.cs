﻿using Shop.Web.Utilities.Interfaces;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class AllBooksFilterExecutor : FilterExecutorBase
    {
        public AllBooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.All;
    }
}