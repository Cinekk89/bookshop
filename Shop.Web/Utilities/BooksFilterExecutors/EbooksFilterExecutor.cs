﻿using Shop.Web.Utilities.Interfaces;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class EbooksFilterExecutor : FilterExecutorBase
    {
        public EbooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.Ebook;

        protected override string CreateAdditionalCondition()
        {
            return "b.ProductTypeId = 2";
        }
    }
}