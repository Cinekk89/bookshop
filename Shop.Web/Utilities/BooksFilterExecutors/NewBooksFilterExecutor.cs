﻿using Shop.Web.Utilities.Interfaces;
using System;
using Shop.Web.Infrastructure;
using Shop.Web.Utilities.Enums;

namespace Shop.Web.Utilities.TabFilters
{
    public class NewBooksFilterExecutor : FilterExecutorBase
    {
        public NewBooksFilterExecutor(IDtoConverter converter) : base(converter)
        {
        }

        public override eBookFilter Filter => eBookFilter.New;

        protected override string CreateAdditionalCondition()
        {
            var dateToCheckIfNewBook = DateTime.Now.AddDays(-14).ToString("yyyyMMdd");
            return "b.ReleaseDate >= '" + dateToCheckIfNewBook + "' AND b.ReleaseDate <= '" + DateTime.Now.ToString("yyyyMMdd") + "'";
        }
    }
}