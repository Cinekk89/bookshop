﻿namespace Shop.Web.Utilities.Enums
{
    // Values must equal to in table t_ProductTypes
    public enum eProductType
    {
        AudioBook = 1,
        EBook = 2
    }
}