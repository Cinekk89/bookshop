﻿namespace Shop.Web.Utilities.Enums
{
    public enum eBookFilter
    {
        All = 0,
        Audiobook = 1,
        Ebook = 2,
        New = 3,
        Announcement = 4,
        GoodDeal = 5
    }
}