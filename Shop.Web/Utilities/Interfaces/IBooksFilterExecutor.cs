﻿using Shop.Web.Models;
using Shop.Web.Utilities.Enums;
using System.Collections.Generic;

namespace Shop.Web.Utilities.Interfaces
{
    public interface IBooksFilterExecutor
    {
        eBookFilter Filter { get; }

        List<BookModel> GetFilteredBooks(BooksQuery query);
    }
}
