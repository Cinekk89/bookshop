﻿using Shop.Web.Models;
using Shop.Web.Models.ViewModels;
using Shop.Web.ShopService;

namespace Shop.Web.Utilities.Interfaces
{
    public interface IDtoConverter
    {
        BookModel ConvertToModel(BookDto dto);
        BooksQueryDto ConvertToDto(BooksQuery baseObject);
        AuthorModel ConvertToModel(AuthorDto dto);
        PublishingHouseModel ConvertToModel(PublishingHouseDto pHouse);
        AddBookModel ConvertToAddModel(BookDto dto);
        CarrierModel ConvertToModel(CarrierDto dto);
    }
}
