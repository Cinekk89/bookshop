﻿using Shop.Web.Utilities.Interfaces;
using Shop.Web.Models;
using Shop.Web.ShopService;
using System;
using Shop.Web.Models.ViewModels;

namespace Shop.Web.Utilities
{
    public class DtoConverter : IDtoConverter
    {
        public BooksQueryDto ConvertToDto(BooksQuery baseObject)
        {
            return new BooksQueryDto()
            {
                Name = baseObject.Name
            };
        }

        public PublishingHouseModel ConvertToModel(PublishingHouseDto pHouse)
        {
            return new PublishingHouseModel()
            {
                Id = pHouse.Id,
                Name = pHouse.Name
            };
        }

        public AuthorModel ConvertToModel(AuthorDto dto)
        {
            return new AuthorModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Surname = dto.Surname,
                FullName = dto.Name + " " + dto.Surname
            };
        }

        public BookModel ConvertToModel(BookDto dto)
        {
            return new BookModel()
            {
                Id = dto.Id,
                Name = dto.Name,
                Price = dto.Price,
                ReleaseDate = dto.ReleaseDate,
                AuthorId = dto.AuthorId,
                Author = ConvertToModel(dto.Author),
                PublishingHouseId = dto.PublishingHouseId,
                PublishingHouse = ConvertToModel(dto.PublishingHouse),
                IsGoodProduct = dto.IsGoodProduct,
                ProductTypeId = dto.ProductTypeId
            };
        }

        public AddBookModel ConvertToAddModel(BookDto dto)
        {
            return new AddBookModel()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public CarrierModel ConvertToModel(CarrierDto dto)
        {
            return new CarrierModel()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }
    }
}