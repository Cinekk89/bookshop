﻿using Shop.Web.Models;
using Shop.Web.Models.ViewModels;
using Shop.Web.Services.Interfaces;
using Shop.Web.Utilities.Enums;
using Shop.Web.Utilities.Extensions;
using Shop.Web.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Shop.Web.Services
{
    public class BooksService : IBooksService
    {
        private readonly IDtoConverter _converter;
        private readonly IEnumerable<IBooksFilterExecutor> _booksFilterExecutors;

        public BooksService(IDtoConverter converter, IEnumerable<IBooksFilterExecutor> booksFilterExecutors)
        {
            _converter = converter;
            _booksFilterExecutors = booksFilterExecutors;
        }

        ShopService.ShopServiceClient client;
        public List<BookModel> GetAllBooks()
        {
            //_tabFilters.First(f => f.Filter == eBookFilter.All).GetFilteredBooks();

            List<BookModel> books = null;
            try
            {
                using (var client = new ShopService.ShopServiceClient())
                {
                    books =  client.GetAllBooks().Select(b => _converter.ConvertToModel(b)).ToList();

                    client.Close();
                }

                
            }catch(Exception e)
            {
                throw;
            }
            finally
            {
                if (client != null)
                    client.Abort();
            }
            
            return books;
        }

        public List<BookModel> GetFilteredBooks(BooksQuery query, eBookFilter filterType)
        {
            var executor = _booksFilterExecutors.First(f => f.Filter == filterType);

            if (query == null)
                query = new BooksQuery();

            var books = executor.GetFilteredBooks(query);

            return books;
        }

        public AddBookModel GetBookForAdd(int id)
        {
            AddBookModel book = null;

            using (var client = new ShopService.ShopServiceClient())
            {
                try
                {
                    var bookDto = client.GetBook(id);

                    book = _converter.ConvertToAddModel(bookDto);

                    book.Carriers = GetCarriersList(true);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    if (client != null)
                        client.Abort();
                }
            }

            return book ?? new AddBookModel();
        }

        public void AddBookToCart(AddBookModel model, HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var booksInCart = GetBooksInCart(context);

            var alreadyStoredBook = booksInCart.Books.FirstOrDefault(b => b.Id == model.Id && b.CarrierId == model.CarrierId);

            if(alreadyStoredBook != null)
            {
                alreadyStoredBook.Amount += model.Amount;
            }else
            {
                booksInCart.Books.Add(model);
            }

            string booksSerialized = new JavaScriptSerializer().Serialize(booksInCart);
            var cookie = new HttpCookie("cartProducts")
            {
                Expires = DateTime.Now.AddDays(3)
            };
            cookie.SetEncodedValue(booksSerialized);

            if (context.Request.Cookies["cartProducts"] == null)
                //context.Response.Cookies.Add(cookie);
                context.Response.SetCookie(cookie);
            else
                context.Response.SetCookie(cookie);
        }

        public BooksCartModel GetBooksInCart(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            BooksCartModel booksInCart = null;

            var cookie = context.Request.Cookies["cartProducts"];

            if (cookie != null)
            {
                booksInCart = new JavaScriptSerializer().Deserialize<BooksCartModel>(cookie.DecodedValue());
            }else
            {
                booksInCart = new BooksCartModel();
            }

            return booksInCart;

        }

        public List<CarrierModel> GetCarriersList(bool withEmpty)
        {
            List<CarrierModel> carriers = null;

            using (var client = new ShopService.ShopServiceClient())
            {
                try
                {
                    var carriersDto = client.GetCarriers();

                    carriers = carriersDto.Select(c => _converter.ConvertToModel(c)).ToList();

                    if (carriers != null && withEmpty)
                        carriers.Insert(0, new CarrierModel() { Id = 0, Name = "-- Wybierz --" });
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    if (client != null)
                        client.Abort();
                }
            }

            return carriers;
        }
    }
}