﻿using Shop.Web.Models;
using Shop.Web.Models.ViewModels;
using Shop.Web.Utilities.Enums;
using System.Collections.Generic;
using System.Web;

namespace Shop.Web.Services.Interfaces
{
    public interface IBooksService
    {
        List<BookModel> GetAllBooks();
        List<BookModel> GetFilteredBooks(BooksQuery query, eBookFilter filterType);
        AddBookModel GetBookForAdd(int id);
        void AddBookToCart(AddBookModel model, HttpContext context);
        BooksCartModel GetBooksInCart(HttpContext context);

        List<CarrierModel> GetCarriersList(bool withEmpty);
    }
}